<?php

namespace Zubrsoft\StatisticBundle\DependencyInjection;

class InfluxExtension extends \Symfony\Component\DependencyInjection\Extension\Extension
{
    /**
     * @param array $configs
     * @param \Symfony\Component\DependencyInjection\ContainerBuilder $container
     * @throws \Exception
     */
    public function load(array $configs, \Symfony\Component\DependencyInjection\ContainerBuilder $container)
    {
        $loader = new \Symfony\Component\DependencyInjection\Loader\YamlFileLoader(
            $container,
            new \Symfony\Component\Config\FileLocator(__DIR__ . '/../Resources/config')
        );

        $loader->load('services.yml');
    }
}