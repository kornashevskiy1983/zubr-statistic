<?php


namespace Zubrsoft\StatisticBundle\src\Influx;

use InfluxDB\Client;
use InfluxDB\Database;
use InfluxDB\Database\RetentionPolicy;

class InfluxConnection
{
    /** @var Client */
    private $httpClient;
    /** @var Client */
    private $dsnClient;
    /** @var Database */
    private $database;

    const HOST = 'influxdb.docker';
    const PORT = '8086';
    const DB_NAME = 'statistics_db';

    /**
     * InfluxConnection constructor.
     * @throws Client\Exception
     * @throws Database\Exception
     */
    public function __construct()
    {
        $this->httpClient = new Client(self::HOST, self::PORT, 'admin', 'admin');
        $this->database = $this->createDatabase();
        $database = Client::fromDSN(sprintf('influxdb://admin:admin@%s:%s/%s', self::HOST, self::PORT, self::DB_NAME));
        $this->dsnClient = $database->getClient();

    }

    /**
     * @return Database
     * @throws Database\Exception
     */
    public function getDatabase(): Database
    {
        if (!$this->database instanceof Database) {
            $this->database = $this->createDatabase();
        }

        return $this->database;
    }

    /**
     * @return Client
     */
    public function getHttpClient()
    {
        return $this->httpClient;
    }

    /**
     * @return Client
     */
    public function getDSNClient()
    {
        return $this->dsnClient;
    }

    /**
     * @return \InfluxDB\Database
     * @throws \InfluxDB\Database\Exception
     */
    public function createDatabase()
    {
        /** @var Client $client */
        $client = $this->getHttpClient();

        $database = $client->selectDB(self::DB_NAME);

        if (!$database->exists()) {
            $database->create(new RetentionPolicy('statistics'));
        }

        return $database;
    }

    /**
     * @return \InfluxDB\Query\Builder
     */
    public function getQueryBuilder()
    {
        return $this->database->getQueryBuilder();
    }
}