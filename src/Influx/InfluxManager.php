<?php

namespace Zubrsoft\StatisticBundle\src\Influx;

abstract class InfluxManager
{
    /** @var InfluxConnection */
    protected $influxConnection;
    /** @var string */
    protected $measurement;
    /** @var array */
    protected $fields;
    /** @var InfluxManager */
    protected static $instance;

    /**
     * InfluxManager constructor.
     * @throws \InfluxDB\Client\Exception
     * @throws \InfluxDB\Database\Exception
     */
    public function __construct()
    {
        $this->influxConnection = new InfluxConnection();
    }

    /**
     * @return InfluxConnection
     */
    public function getInfluxConnection()
    {
        return $this->influxConnection;
    }

    /**
     * @param string $measurement
     * @return InfluxManager
     */
    public function setMeasurement(string $measurement): self
    {
        $this->measurement = $measurement;

        return $this;
    }

    /**
     * @param array $fields
     * @return InfluxManager
     */
    public function setFields(array $fields): self
    {
        $this->fields = $fields;

        return $this;
    }

    /**
     * @return string
     */
    public function getMeasurement(): string
    {
        return $this->measurement;
    }

    /**
     * @return array
     */
    public function getFields(): array
    {
        return $this->fields;
    }

    abstract public function write(array $points);
    abstract public static function getInstance();
}