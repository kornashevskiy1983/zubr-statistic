<?php


namespace Zubrsoft\StatisticBundle\src\Influx;


use InfluxDB\Database;

class InfluxTCP extends InfluxManager
{
    /**
     * @return InfluxManager|InfluxTCP
     * @throws Database\Exception
     * @throws \InfluxDB\Client\Exception
     */
    public static function getInstance()
    {
        if (!isset(self::$instance)) {
            self::$instance = new self;
        }

        return self::$instance;
    }

    /**
     * @param array $points
     * @return bool
     * @throws \InfluxDB\Exception
     */
    public function write(array $points)
    {
        /** @var Database $database */
        $database = $this->getInfluxConnection()->getDatabase();

        $result = $database->writePoints($points, Database::PRECISION_MILLISECONDS);

        return $result;
    }
}