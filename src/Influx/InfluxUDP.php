<?php


namespace Zubrsoft\StatisticBundle\src\Influx;


use InfluxDB\Client;
use InfluxDB\Database;
use InfluxDB\Driver\UDP;

class InfluxUDP extends InfluxManager
{
    /**
     * @return InfluxManager|InfluxUDP
     * @throws Client\Exception
     * @throws Database\Exception
     */
    public static function getInstance()
    {
        if (!isset(self::$instance)) {
            self::$instance = new self;
        }

        return self::$instance;
    }

    /**
     * @param array $points
     * @return bool
     * @throws Database\Exception
     * @throws \InfluxDB\Exception
     */
    public function write(array $points)
    {
        /** @var Client $client */
        $client = $this->getInfluxConnection()->getDSNClient();
        /** @var Database $database */
        $database = $this->getInfluxConnection()->getDatabase();

        $client->setDriver(new UDP($client->getHost(), 4444));

        $result = $database->writePoints($points, Database::PRECISION_MILLISECONDS);

        return $result;
    }

}