<?php

namespace Zubrsoft\StatisticBundle\src\Stopwatch;

use Symfony\Component\Stopwatch\Stopwatch;

class StopwatchManager
{
    private static $stopwatch;

    const CONTROLLER_MEASUREMENT = 'controller';

    public static function getInstance()
    {
        if (!isset(self::$stopwatch)) {
            self::$stopwatch = new Stopwatch();
        }

        return self::$stopwatch;
    }
}